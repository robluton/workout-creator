
export function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

export function generateId(prefix, start) {
    var i = start || 0;
    return function() {
        return prefix + i++;
    }
}

export function getStore(key = 'store') {
    return JSON.parse(localStorage.getItem(key));
}

export function updateStore(updatedStore, key = 'store') {
    const stringifiedStore = JSON.stringify(updatedStore);
    localStorage.setItem(key, stringifiedStore);
}

export function saveDefaultConfig() {
    let config = {
        day: 1,
        totalDays: 14,
        supersets: {
            byId: {
                1: [
                    {
                        id: 1,
                        label: 'Pull-ups',
                        tranversePlane: 'upper',
                        maxReps: 10,
                        algorithm: 'ladder'
                    }
                ],
                2: [
                    {
                        id: 2,
                        label: 'Squats',
                        tranversePlane: 'lower',
                        maxReps: 35,
                        algorithm: 'pyramid'
                    },
                    {
                        id: 3,
                        label: "push-ups",
                        tranverseplane: 'upper',
                        maxReps: 25,
                        algorithm: 'reverseLadder'
                    }
                ]
            },
            ids: [1, 2]
        }
    }

    localStorage.setItem('store', JSON.stringify(config));
}

