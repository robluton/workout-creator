import React, { Component } from 'react';
import *  as util from './util/util.js'
import _ from 'lodash';
import SupersetForm from './components/SupersetForm.jsx';
import ExerciseForm from './components/ExerciseForm.jsx';
import Superset from './components/Superset.jsx';
import Modal from './components/Modal.jsx';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import './App.css';

class Settings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            totalDays: 14 
        }

        this.durationOptions = [
            {label: '1 Week', value: 7},
            {label: '2 Weeks', value: 14},
            {label: '3 Weeks', value: 21},
            {label: '1 Month', value: 28}
        ];

        this.onChange = this.onChange.bind(this);
    }

    onChange(selection) {
        this.setState({totalDays: selection.value})
    }

    render() {
        console.log("test");
        return <div className="row">
            <div className="col-xs-12">
                <label>Workout Duration</label> 
                <Select clearable={false} onChange={this.onChange} value={this.state.totalDays} options={this.durationOptions} />
            </div>
            <div className="col-xs-12">
                <button className="btn btn-primary" onClick={(e) => this.props.save(e, this.state)}>Save</button>
            </div>
        </div>
    }
}

class App extends Component {

    constructor(props) {
        super(props);

        let store = util.getStore() || {
            totalDays: 14,
            day: 1,
            supersets: {
                byId: {},
                ids: []
            }
        };
        const algorithms = {
            pyramid: [0.4, 0.6, 0.8, 0.6, 0.4],
            inverted_pyramid: [0.8, 0.6, 0.4, 0.6, 0.8],
            ladder: [0.2, 0.4, 0.5, 0.6, 0.7],
            reverse_ladder: [0.7, 0.6, 0.5, 0.4, 0.2]
        }

        this.state = {
            store,
            algorithms, //each value multiplied by maximumReps for each exercise
            setCount: 5, //must be equal to algorithm arrays
            modalView: '',
            showWorkout: (store.supersets.ids.length > 0 ? true : false)
        }
        this.showWorkout = this.showWorkout.bind(this);
        this.showSettings = this.showSettings.bind(this);
        this.showEditForm = this.showEditForm.bind(this);
        this.removeSuperset = this.removeSuperset.bind(this);
        this.saveSuperset = this.saveSuperset.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.saveSettings = this.saveSettings.bind(this);

    }
    saveSuperset(e, data, id) { 
        e.preventDefault();
        let currentState = JSON.parse(JSON.stringify(this.state.store));
        let supersetId = 1;
        let superset = null;
        if(!id) {
            //creating new superset. generate id.
            let currentIds = Object.values(currentState.supersets.ids) || null;
            if(currentIds && currentIds.length > 0) {
                let startIndex = Math.max(...currentIds) + 1;
                id = util.generateId(startIndex);
                supersetId = id();
            }
            currentState.supersets.ids.push(supersetId);
            superset = data;
        } else {
            supersetId = id;
            superset = currentState.supersets.byId[supersetId] = data;
            //todo: merge with new data
        }
        currentState.supersets.byId[supersetId] = superset;
        util.updateStore(currentState);
        this.setState({store: currentState, modalView: ''});
    }
    removeSuperset(e, id) {
        e.preventDefault();
        let currentState = JSON.parse(localStorage.getItem('store'));
        let currentIds = Object.values(currentState['supersets'].ids);

        currentIds = currentIds.reduce(function(result, currentId) {
            if(id === currentId) {
                delete currentState.supersets[id];
            } else {
                result.push(currentId);
            }
            return result;
        }, []);

        currentState['supersets'].ids = currentIds;

        util.updateStore(currentState);
        this.setState({store: currentState});
    }
    updateConfig(newConfig) {
        const mergedConfig = _.assign({}, this.state.store, newConfig);
        localStorage.setItem('store', mergedConfig);
    }
    showDay(e, day) {
        e.preventDefault();
        let newState = _.assign({}, this.state.store);
        newState.day = day;
        this.setState({store: newState});
    }
    getWorkoutProgram(numberOfDays) {
        let self = this;
        let workouts = Array(numberOfDays).fill().reduce(function(result, num, i) {
            if(i + 1 === self.state.store.day) {
                result = <div key={num}>{self.workoutDay(i + 1)}</div>;
            }
            return result;
        }, {});

        let nav = Array(numberOfDays).fill().map(function(num, i) {
            let style = {display: 'inline-block', padding: '5px'};
            if(this.state.store.day === (i + 1)) {
                style.background = '#efefef';
            }
            return <div 
                key={i}
                onClick={(e) => self.showDay(e, i + 1)} 
                style={style}>
                    <a href='' style={{textDecoration: 'none'}}>{i + 1}</a>
                </div>
        }, this);

        return <div>
            <div style={{textAlign: 'center'}}>
                <h3>Your Workout Program</h3>
                Day: {nav}
            </div>
            <div>
                <button className="btn btn-primary btn-border" style={{marginRight: '5px'}} onClick={(e) => this.showCreateForm(e, 'exercise')}>Add Exercise</button>
                <button className="btn btn-primary btn-border" onClick={(e) => this.showCreateForm(e, 'superset')}>Add Superset</button>
            </div>

            {workouts}
        </div>
    }

    prepareExerciseData(exercise, day) {
        let label = exercise.label;
        let addend = (day * 2 * .01);
        
        let algorithm = exercise.algorithm && this.state.algorithms[exercise.algorithm] ? exercise.algorithm : 'pyramid';
        let sets = this.state.algorithms[algorithm].map(function(percentageOfMax, i) {
            let baseCount = exercise.maxReps * percentageOfMax
            return Math.round((baseCount * addend) + baseCount);
        });

        let totalReps = sets.reduce(function(result, repsForSet) {
            result += parseInt(repsForSet, 10);
            return result;
        }, 0);

        return {id: exercise.id, label, sets, totalReps}
    }
    getRoutine(workoutData, day) {
        const self = this;

        if(workoutData.supersets.ids.length < 1) {
            return; 
        }
        return workoutData.supersets.ids.reduce(function(result, supersetId) {
            let superset = workoutData.supersets.byId[supersetId];
            let supersetData = superset.map((exercise) => {
                return self.prepareExerciseData(exercise, day);
            });

            result.push({id: supersetId, exercises: supersetData});

            return result
        }, []);
    }
    showEditForm(e, id) {
        e.preventDefault();
        this.setState({modalView: 'editExercise', selected: id});
    }
    showCreateForm(e, type) {
        e.preventDefault();
        this.setState({modalView: `create${type}`});
    }
    saveUserData(data) {
        localStorage.setItem('userConfig', JSON.stringify(data));
        localStorage.getItem('myData');
    }
    closeModal(e) {
        e.preventDefault();
        this.setState({modalView: '', selected: null});
    }
    showWorkout(e) {
        this.setState({showWorkout: true});
    }

    showSettings(e) {
        e.preventDefault();
        this.setState({modalView: 'editSettings'});
    }
    
    /*
     * @param day integer
    */
    workoutDay(day) {
        let routine = this.getRoutine(this.state.store, day) || [];


        const self = this;

        let workoutDay = routine.reduce(function(result, superset, supersetIterator) {
            let superSetTotal = superset.exercises.map((exercise, i) => {
                return <div>
                    <div className="exercise-label">{exercise.label}</div>
                    <div className="exercise-reps">{exercise.totalReps}</div>
                </div>
            });
            result.totals.push(superSetTotal);
            result.workoutItems.push(<Superset 
                key={superset.id} 
                heading={`Step ${supersetIterator + 1}`} 
                exercises={superset.exercises} 
                onEdit={self.showEditForm}
                onRemove={self.removeSuperset}
                id={superset.id}
                numberOfSets={5} />
            );

            return result;
        }, {totals: [], workoutItems: []});

        return (
            <div>
                {/*<div className="row">
                    <div className="col-xs-12">
                        <h4>Day {day}</h4>
                        <div className="app_total-reps">
                            <div>{workoutDay.totals}</div>
                        </div>
                    </div>
                </div>
                */}
                {workoutDay.workoutItems}
            </div>
        );
    }

    saveSettings(e, settings) {
        e.preventDefault();
        let currentState = JSON.parse(JSON.stringify(this.state.store));
        currentState.totalDays = parseInt(settings.totalDays, 10);
        this.setState({store: currentState, modalView: '', showWorkout: true});
        util.updateStore(currentState);
    }
    render() {
        let modal = null;
        let modalBody = null;
        let modalTitle = null;

        let algorithmOptions = [
            {label: 'Pyramid', value: 'pyramid' },
            {label: 'Inverted Pyramid', value: 'inverted_pyramid'},
            {label: 'Ladder', value: 'ladder'},
            {label: 'Reverse Ladder', value: 'reverse_ladder'},
        ];
               
        if(this.state.modalView === 'editSettings') {
            modalTitle = 'Edit Settings';
            modalBody = <Settings save={this.saveSettings} />;
        }
        if(this.state.modalView === 'editExercise') {
            if(this.state.store.supersets.byId[this.state.selected].length > 1) {
                modalTitle = 'Edit Superset';
                modalBody = <SupersetForm 
                    id={this.state.selected} 
                    exercises={this.state.store.supersets.byId[this.state.selected]}
                    cancel={this.closeModal}
                    save={this.saveSuperset}
                    options={algorithmOptions}
                />;
            } else {
                modalTitle = 'Edit Exercise';
                modalBody = <ExerciseForm 
                    id={this.state.selected} 
                    {...this.state.store.supersets.byId[this.state.selected][0]}
                    cancel={this.closeModal}
                    save={this.saveSuperset}
                    options={algorithmOptions}
                />;
            }
            modal = <Modal title='Edit Exercise' body={modalBody} close={this.closeModal} />;
        }
        if(this.state.modalView === 'createsuperset') {
            modalTitle = 'Add Superset';
            modalBody = <SupersetForm cancel={this.closeModal} save={this.saveSuperset} options={algorithmOptions} />;
        }

        if(this.state.modalView === 'createexercise') {
            modalTitle = 'Add Exercise';
            modalBody = <ExerciseForm cancel={this.closeModal} save={this.saveSuperset} options={algorithmOptions} />;
        }

        if(this.state.modalView) {
            modal = <Modal title={modalTitle || ''} body={modalBody} close={this.closeModal} />;
        }
        

        let content = '';
        if(this.state.showWorkout) {
            content = <div className="col-xs-12">
                {this.getWorkoutProgram(this.state.store.totalDays)}
            </div>;
        } else {
            content = <div className="col-xs-12 text-center">
                <p style={{marginTop: '30px'}}>You're busy. Supersets are fast. This app is simple.</p> 
                <button className="btn btn-primary" onClick={this.showSettings}>Create Your Workout</button>
            </div>;
        }
        return <div>
            <div style={{background: 'ghostwhite', borderBottom: '1px solid #ccc'}}>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-7 text-left">
                            <span style={{color: 'coral', fontSize: '35px'}}>Superset</span> <span style={{color: '#333', fontSize: '18px', fontStyle: 'italic'}}>Me</span><br />
                            <small>Fitness for Busy People</small>
                        </div>
                        <div className="col-xs-5 text-right">
                            <button className="btn btn-primary btn-border" onClick={this.showSettings}>Settings</button> 
                        </div>
                    </div>
                </div>
            </div>

            <div className="container">
            <div>{modal}</div>
            <div className="row">
                {content}
            </div>
        </div>
    </div>
    }
}

export default App;
