import React from 'react';
import ReactDOM from 'react-dom';
import './sass/bootstrap/bootstrap.min.css';
import './sass/bootstrap/bootstrap-material-design.min.css';
import './sass/ion-icons/ionicons.min.css';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
