import React from 'react';

export default function Modal(props) {
    return (
        <div className="modal show">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" onClick={props.close}><span aria-hidden="true">&times;</span></button>
                        <h4 className="modal-title">{props.title}</h4>
                    </div>
                    <div className="modal-body">
                        <div>{props.body}</div>
                    </div>
                    <div className="modal-footer">
                        {props.footer}
                    </div>
                </div>
            </div>
        </div>
    );
}

