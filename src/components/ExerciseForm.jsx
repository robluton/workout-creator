import React from 'react';
import Select from 'react-select';

export default class ExerciseForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            label: props.id ? props.label : '',
            maxReps: props.id ? props.maxReps : '',
            algorithm: 'pyramid'
        };
    }

    onChange(e) {
        e.preventDefault();
        let newState = Object.assign({}, this.state, {[e.target.name]: e.target.value});
        this.setState(newState);
    }

    onChangePattern(selection) {
        this.setState({algorithm: selection.value});
    }

    update(e) {
        e.preventDefault();
        this.props.save(this.state);
    }

    render() {
       return <div className="row">
            <div className="col-xs-12">
                <div style={{background: 'aliceblue', border: '1px solid #ccc', padding: '20px', width: '100%', float: 'left'}}>
                    <div className="row">
                        <div className="col-xs-12"><strong>Enter Exercise</strong></div>
                        <div className="col-sm-6">Exercise Name: 
                            <input className="form-control" name="label" onChange={(e) => this.onChange(e)} type="text" value={this.state.label}/>
                        </div>
                        <div className="col-sm-6">Maximum Reps: 
                            <input className="form-control" name="maxReps" type="number" onChange={(e) => this.onChange(e)} value={this.state.maxReps} />
                        </div>
                        <div className="col-xs-12">Rep Pattern: 
                            <Select clearable={false} options={this.props.options} onChange={(selection) => this.onChangePattern(selection)} value={this.state.algorithm} />
                        </div>
                    </div>
                    <div className="col-xs-12 text-right">
                        <button className="btn btn-secondary" onClick={this.props.cancel}>Cancel</button>
                        <button className="btn btn-primary" onClick={(e) => this.props.save(e, [this.state], this.props.id)}>Save</button> 
                    </div>
                </div>
            </div>
        </div>
    }
}
