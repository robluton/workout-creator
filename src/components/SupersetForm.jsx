import React from 'react';
import Select from 'react-select';

export default class SupersetForm extends React.Component {

    constructor(props) {
        super(props);
        this.initialState = props.exercises 
            ? props.exercises.map((exercise) => {
                return {...exercise};
            }) 
            : null;

        this.state = {
            superset: this.initialState || 
            [
                { label: '', maxReps: '', algorithm: 'pyramid' },
                { label: '', maxReps: '', algorithm: 'pyramid' }
            ]
        }
    }

    onChangePattern(selection, index) {
        let supersetState = this.state.superset.slice();

        let updated = Object.assign({}, this.state.superset[index], {algorithm: selection.value});
        supersetState[index] = updated;

        this.setState({superset: supersetState});
    }

    onChange(e, index) {
        e.preventDefault();

        let supersetState = this.state.superset.slice();

        let updated = Object.assign({}, this.state.superset[index], {[e.target.name]: e.target.value});
        supersetState[index] = updated;

        this.setState({superset: supersetState});
    }

    render() {
       return <div className="row">
            <div className="col-xs-12">
                <div style={{background: 'aliceblue', border: '1px solid #ccc', padding: '20px', width: '100%', float: 'left'}}>
                    <div className="row">
                        <div className="col-xs-12"><strong>Enter Exercise A</strong></div>
                        <div className="col-sm-6">Exercise Name: 
                            <input className="form-control" name="label" onChange={(e) => this.onChange(e, '0')} type="text" value={this.state.superset[0].label}/>
                        </div>
                        <div className="col-sm-6">Maximum Reps: 
                            <input className="form-control" name="maxReps" type="number" onChange={(e) => this.onChange(e, '0')} value={this.state.superset[0].maxReps} />
                        </div>
                        <div className="col-xs-12">Rep Pattern: 
                            <Select clearable={false} options={this.props.options} onChange={(selection) => this.onChangePattern(selection, '0')} value={this.state.superset[0].algorithm} />
                        </div>
                    </div>
                    <div className="row" style={{marginTop: '20px'}}>
                        <div className="col-xs-12"><strong>Enter Exercise B</strong></div>
                        <div className="col-sm-6">Exercise Name: 
                            <input className="form-control" name="label" onChange={(e) => this.onChange(e, '1')} type="text" value={this.state.superset[1].label}/>
                        </div>
                        <div className="col-sm-6">Maximum Reps: 
                            <input className="form-control" name="maxReps" type="number" onChange={(e) => this.onChange(e, '1')} value={this.state.superset[1].maxReps} />
                        </div>
                        <div className="col-xs-12">Rep Pattern: 
                            <Select clearable={false} options={this.props.options} onChange={(selection) => this.onChangePattern(selection, '1')} value={this.state.superset[1].algorithm} />
                        </div>
                    </div>
                    <div className="col-xs-12 text-right">
                        <button className="btn btn-secondary" onClick={this.props.cancel}>Cancel</button>
                        <button className="btn btn-primary" onClick={(e) => this.props.save(e, this.state.superset, this.props.id)}>Save</button> 
                    </div>
                </div>
            </div>
        </div>
    }
}
