import React from 'react';
import * as util from '../util/util.js';

export default (props) => {
    let superSet = [];

    function getSupersetHeadings() {
        let headings = props.exercises.map(function(exercise, i) {
            let cssClass = (i + 1) % 2 === 0 ? 'super_heading-2' : 'super_heading-1';
            return <div key={i} style={{display: 'inline-block', float: 'left', marginRight: '10px'}}>
                    <div style={{float: 'left', display: 'inline-block', marginRight: '5px', lineHeight: '20px'}}>{exercise.label}</div>
                    <div className={cssClass} style={{float: 'left', border: '1px solid #ccc', display: 'inline-block', width: '20px', height: '20px'}}></div>
                </div>
        });

        return <div className="col-xs-12">{headings}</div>
    }

    function getSuperset(exercises, i) {
        return props.exercises.reduce(function(result, exercise, count) {
            let numberOfReps = util.pad(exercise.sets[i], 2);
            let cssClass = (count + 1) % 2 === 0 ? 'super_rep-count-2' : 'super_rep-count-1';
            
            result.push(
                <div  key={exercise.label + i}>
                    <div className={cssClass}>{numberOfReps}</div>
                </div>);
            return result;
        }, []);
    }

    let supersetHeadings = getSupersetHeadings();
    for(var i = 0; i < props.numberOfSets; i++) {
        let set = getSuperset(props.exercises, i);
        let setGroup = <div key={i} className="col-xs-4 col-sm-2">
            <div style={{textAlign: 'center', marginBottom: '5px'}}>Set {i + 1}</div>
            <div style={{border: '1px solid #ccc', marginTop: '5px', padding: '5px', textAlign: 'center'}}>
                {set}
            </div>
            </div>;
        superSet.push(setGroup);
    }
    return <div className="row" style={{marginBottom: '5px'}}>
        <div className="col-xs-12">
            <div style={{background: 'lightsteelblue', color: '#fff', fontSize: '18px', padding: '0 10px', marginBottom: '10px', lineHeight: '56px'}}>
                <div className="row">
                    <div className="col-xs-6">
                        {props.heading}
                    </div>
                    <div className="col-xs-6 text-right">
                        <button className="btn btn-sm btn-primary" onClick={(e) => props.onEdit(e, props.id)}>Edit</button>
                        <button className="btn btn-sm btn-alert" onClick={(e) => props.onRemove(e, props.id)}>Delete</button>
                    </div>
                </div>
            </div>
            <div style={{padding: '10px'}}>
                <div className="row" style={{marginBottom: '20px'}}>
                        {supersetHeadings}
                </div>
                <div className="row" style={{marginTop: '5px', marginBottom: '20px'}}>
                        {superSet}
                </div>
            </div>
        </div>
    </div>;
}

